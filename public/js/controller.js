/*
	 * Handling Logic (Controller)
	 *
	 * These objects handle the business logic of our app. The logic
	 * in this case is "start quiz", "next question" and "end quiz".
	 */

	/*
	 * The QuizApp object coordinates all the other objects in the
	 * application, and controls the flow of the quiz.
	 */
	class QuizApp 
	{
		constructor(data)
		{
			this.data = data;
			this.introView = new IntroView('#quiz-intro', this);
			this.outroView = new OutroView('#quiz-outro', this);
			this.questionView = new QuestionView('#quiz-form', this);
			
			this.introView.attachEventHandlers();
			this.outroView.attachEventHandlers();
			this.questionView.attachEventHandlers();
		}


	startQuiz() 
	{
		this.quiz = new Quiz(this.data);

		this.introView.toggle(true);
		this.outroView.toggle(true);
		this.questionView.toggle(false);

		this.nextQuestion();
	}

	checkValid(answer)
	{
		let self = this;
		let tempCounter = self.quiz.counter - 1;

		let allAnswers = document.querySelectorAll(".answers-container li");
		const noticeBig = document.getElementById("noticeAnswer");

		for (let i=0; i<allAnswers.length; i++) {
 			allAnswers[i].style.color = "#797F7C";
		}

		if (this.quiz.validateAnswer(answer))
		{
			allAnswers[answer].style.color = "green";
			noticeBig.innerHTML = "✔ Bonne réponse !";
			noticeBig.style.color = "green";
			document.getElementById("noticeInfos").innerHTML = self.quiz.questions[tempCounter].moreInformation;
		}
		else {
			allAnswers[answer].style.color = "red";
			allAnswers[self.quiz.questions[tempCounter].correctIndex].style.color = "darkgreen";
			noticeBig.innerHTML = "✗ Mauvaise réponse !";
			noticeBig.style.color = "red";
			document.getElementById("noticeInfos").innerHTML = self.quiz.questions[tempCounter].moreInformation;
		}
		document.getElementById("noticeGroup").classList.toggle("hidden", false);
		$("input[type=radio]").attr('disabled', true);
		if (tempCounter == 6) {
			this.questionView.submitAnswerButton[0].innerHTML = "Afficher les résultats »";
		}
		else {
			this.questionView.submitAnswerButton[0].innerHTML = "Suivant »";
		}
		this.questionView.status = 0;

		return answer;
	}

	nextQuestion(answer) 
	{
		var nextQuestion = this.quiz.advanceQuestion(answer);
		
		if (nextQuestion) 
		{
			this.questionView.setQuestion(nextQuestion);
		} 
		else 
		{
			this.endQuiz();
		}
	}

	endQuiz() 
	{
		this.questionView.toggle(true);
		this.outroView.toggle(false);
		this.outroView.displayOutroMessage(this.quiz.numberCorrect, this.quiz.questions.length);
	}
}
