	/*
	 * Handling Data (Model):
	 *
	 * These objects handle all of the data for the app. In this
	 * case our data are in the form of quiz questions, where each
	 * question has a prompt, a set of answers, and a correct answer.
	 */
	
	/*
	 * The Question object represents a single question. It has
	 * properties that reflect the data, and a set of methods
	 * to interact with that data.
	 */

class Question
{
    constructor(datum)
    {
        this.prompt = datum.prompt;
        this.answers = datum.answers;
        this.correctIndex = datum.correctIndex;
        this.moreInformation = datum.moreInformation;
    }

    checkAnswer(index) 
    {
        return index === this.correctIndex;
    }

    forEachAnswer(callback, context)
    {
        this.answers.forEach(callback, context);
    }
}