	/*
	 * Handling Presentation (View):
	 *
	 * These objects handle all of the manipulation of the DOM as well
	 * as handling events triggered on the DOM. We have three views, one
	 * for each section of the application.
	 */

	/*
	 * The IntroView handles interaction with the #quiz-intro section
	 * and its .start-button. When the start button is clicked it
	 * starts the quiz by interacting with its QuizApp object through
	 * the startQuiz method. It also implements methods to attach
	 * event handlers and toggle its visibility.
	 */
    class IntroView 
    {
        constructor(selector, quizApp)
        {
            this.element = $(selector);
            this.startButton = this.element.find('.start-button');
            this.quizApp = quizApp;
        }

	

    attachEventHandlers() 
    {
		var self = this;

		this.startButton.click(function() {
			self.quizApp.startQuiz();
		});
	}

    toggle(hide) 
    {
		this.element.toggleClass('hidden', hide);
	}
}
	