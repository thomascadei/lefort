/*
	 * The OutroView is similar to the IntroView, with the addition
	 * of a displayOutroMessage method which displays an appropriate
	 * message based on the number of correct answers and the total
	 * number of questions.
	 */
    class OutroView 
    {
        constructor(selector, quizApp)
        {
            this.element = $(selector);
            this.resetButton = this.element.find('.reset-button');
            this.outroMessage = this.element.find('.quiz-outro-message');
            this.quizApp = quizApp;
        }

        displayOutroMessage(numberCorrect, totalQuestions) {
            var message = 'Vous avez trouvé ' + numberCorrect + ' réponse(s) sur ' + 
                totalQuestions + '. Vous pouvez retenter la chance avec le bouton ci-dessous !';
    
            this.outroMessage.html(message);
        }
    
        attachEventHandlers() {
            var self = this;
    
            this.resetButton.click(function() {
                self.quizApp.startQuiz();
            });
        };
    
        toggle(hide) {
            this.element.toggleClass('hidden', hide);
        }
}