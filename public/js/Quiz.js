/*
	 * The Quiz object is a collection of question objects.
	 * It creates the questions from data, stores the questions,
	 * and keeps track of what question you're on and how many
	 * questions you've gotten right.
	 */

class Quiz {

    constructor(data)
    {
		this.numberCorrect = 0;
		this.counter = 0;
		this.questions = [];
		this.addQuestions(data);
    }

    addQuestions(data)
    {
        for (var i = 0; i < data.length; i++) 
        {
			this.questions.push(new Question(data[i]));
		}
	}

    advanceQuestion(lastAnswer) 
    {
        if (this.currentQuestion && this.currentQuestion.checkAnswer(lastAnswer)) 
        {
			this.numberCorrect++;
		}

		this.currentQuestion = this.questions[this.counter++];

		return this.currentQuestion;
    }

    validateAnswer(lastAnswer)
    {
    	return this.currentQuestion.checkAnswer(lastAnswer);
    }
}