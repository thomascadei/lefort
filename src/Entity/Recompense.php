<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RecompenseRepository")
 */
class Recompense
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cuvee;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $millesime;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $gratification;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCuvee(): ?string
    {
        return $this->cuvee;
    }

    public function setCuvee(string $cuvee): self
    {
        $this->cuvee = $cuvee;

        return $this;
    }

    public function getMillesime(): ?int
    {
        return $this->millesime;
    }

    public function setMillesime(?int $millesime): self
    {
        $this->millesime = $millesime;

        return $this;
    }

    public function getGratification(): ?string
    {
        return $this->gratification;
    }

    public function setGratification(string $gratification): self
    {
        $this->gratification = $gratification;

        return $this;
    }
}
