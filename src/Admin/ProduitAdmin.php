<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Vich\UploaderBundle\Form\Type\VichImageType;

final class ProduitAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('titre', TextType::class, ['label' => 'Cuvée / Titre']);
        $formMapper->add('appellation', TextType::class, ['label' => 'Appellation / Sous-Titre', 'required'   => false]);
        $formMapper->add('millesime', TextType::class, ['label' => 'Millésime (un seul nombre)', 'required'   => false]);
        $formMapper->add('cepage', TextType::class, ['label' => 'Cépage(s) / Variété(s)', 'required'   => false]);
        $formMapper->add('description', TextType::class, ['label' => 'Courte description']);
        $formMapper->add('texte', TextType::class, ['label' => 'Longue description']);
        $formMapper->add('contenance', TextType::class, ['label' => 'Contenance (EN CENTILITRES)']);
        $formMapper->add('prix', NumberType::class, [
            'label' => 'Prix (EN CENTIMES)', 
            'scale' => 0, 
            ]);
        $formMapper->add('classe', TextType::class, ['label' => 'Classe (ROUGE | ROSE | BLANC | OUTRE | DOUCEUR | OLIVE)']);
        $formMapper->add('imageFile', VichImageType::class, [
            'required' => false,
            'allow_delete' => true,
            'download_label' => '...',
            'download_uri' => true,
            'image_uri' => true,
            'label' => 'Image'
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('classe');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('titre');
    }
}