$(function() {
    var datum =
    [
        {
            "prompt": "Quels arômes retrouve-t-on dans les vins issus du cépage syrah ?",
            "answers":
            [
                "mûre-violette-poivre",
                "cerise-aubépine-fumé",
                "cassis-tabac-noisette"
            ],
            "correctIndex": 0,
            "moreInformation": "L’examen organoleptique de la syrah révèle des arômes de mûre et d’épices poivrées, souvent agrémentés d’une pointe de violette. Avec l’âge, les arômes évoluent vers des notes de pruneau et de cuir."
        },
        {
            "prompt": "Quels sont les trois principaux cépages de l’AOP Champagne ?",
            "answers":
            [
                "chardonnay-pinot gris-chenin",
                "chardonnay-pinot meunier-pinot noir",
                "chardonnay-sauvignon-gamay"
            ],
            "correctIndex": 1,
            "moreInformation": "Sept cépages sont autorisés en AOP Champagne. Chardonnay, pinot meunier et pinot noir représentent 99% de l’encépagement total.\n L’arbanne, le petit meslier, le pinot blanc et le pinot gris se répartissent les 1% restant."
        },
        {
            "prompt": "Quelle information révèle l’observation des jambes du vin ?",
            "answers":
            [
                "la teneur en gras et en alcool",
                "la teneur en acidité et en sucre",
                "l’âge et la puissance"
            ],
            "correctIndex": 0,
            "moreInformation": "Les jambes du vin, également appelées larmes ou cuisses, sont les traces transparentes qui se forment sur les parois intérieures d’un verre lorsque l’on fait tourner le vin pour l’observer.\n Plus elles sont épaisses et lentes à s’écouler, plus le vin est riche en alcool et en glycérol, signe révélateur de la maturité des raisins utilisés."
        },
        {
            "prompt": "D’ordinaire interdite, quelle étrange pratique est caractéristique de l’AOP Côte-Rôtie ?",
            "answers":
            [
                "les fûts d’élevage sont fabriqués en bois de cèdre",
                "du sucre de betterave est ajouté pour augmenter le taux d’alcool",
                "le vin rouge est vinifié à partir de 20% de raisin blanc"
            ],
            "correctIndex": 2,
            "moreInformation": "Le viognier est un cépage emblématique des Côtes du Rhône. Il est le seul autorisé pour l’AOP Condrieu.\n À quelques kilomètres de là, on trouve l’appellation Côte-Rôtie. Ici, le viognier est assemblé à la syrah (dans une proportion maximum de 20%) et le résultat est somptueux : un apport de moelleux, de fraîcheur et d’élégance !"
        },
        {
            "prompt": "Les cépages Riesling et Sauvignon ont une certaine vivacité… Mais à quelle particularité du vin ce terme fait-il référence ?",
            "answers":
            [
                "son taux d’alcool",
                "sa couleur",
                "son acidité"
            ],
            "correctIndex": 2,
            "moreInformation": "Le vocabulaire employé pour décrire un vin est constitué de termes précis qu’il faut savoir utiliser à bon escient. Ainsi, un vin est dit “vif” quand sa saveur acide domine agréablement les autres. On associe cet adjectif à la fraîcheur et à la légèreté.\n Par exemple, la dégustation des vins de cépage Riesling et Sauvignon Blanc du Domaine LE FORT révèlent une franche vivacité."
        },
        {
            "prompt": "Le vin est obtenu par la fermentation alcoolique du jus de raisin. Mais comment s’appelle le deuxième processus de fermentation qui permet de stabiliser et assouplir les vins rouges ?",
            "answers":
            [
                "fermentation malo-lactique",
                "fermentation acétique",
                "fermentation glucidique"
            ],
            "correctIndex": 0,
            "moreInformation": "Particulièrement recherchée dans la vinification des vins rouges, la fermentation malolactique est la transformation de l'acide malique en acide lactique par l'intermédiaire de bactéries.\n Elle permet notamment de diminuer l’acidité et de stabiliser l’évolution du vin. Elle peut se réaliser précocement, simultanément à la fermentation alcoolique ; ou tardivement, pendant l’élevage. "
        },
        {
            "prompt": "La langue de l’être humain détecte et distingue cinq saveurs : sucré, salé, acide, amer et ... ?",
            "answers":
            [
                "aigre",
                "umami",
                "calcium"
            ],
            "correctIndex": 1,
            "moreInformation": "Umami signifie “savoureux” en japonais. C’est la saveur du glutamate, du bouillon, ou de la sauce soja par exemple. On la retrouve dans de nombreux aliments, du lait maternel aux champignons, en passant par le fromage ou les crustacés.\n L’aigreur est en fait l’équivalent de l’acidité, dans une notion péjorative. La détection de la saveur calcium, présente dans le choux et le pavot, serait impossible pour l’être humain, mais pas pour la souris !"
        }
       
        
    ]
    var quizApp = new QuizApp(datum);
});