/*
 * The QuestionView is where most of the action is. It has similar methods
 * that attach event handlers and toggle the element visibility. It also
 * implements a setQuestion method that takes a question and generates
 * the HTML for the prompt and answers and puts them into the DOM.
 */

class QuestionView {
	constructor(selector, quizApp) {
		this.element = $(selector);
		this.submitAnswerButton = this.element.find('.submit-answer-button');
		this.questionContainer = this.element.find('.question-container');
		this.answersContainer = this.element.find('.answers-container');
		this.status = 1;
		this.quizApp = quizApp;
		this.trueAnswer;
	}

	attachEventHandlers() {
		var self = this;

		this.submitAnswerButton.click(function () {
			var checkedInput = self.answersContainer.find('input:checked');

			if (!checkedInput.length) {
				alert('Merci de choisir une réponse.');
			}
			else {
				var answer = +checkedInput.val();

				if (self.status == 1) { // "Valider"
					self.trueAnswer = self.quizApp.checkValid(answer);
				}

				else { // "Continuer"
					self.submitAnswerButton[0].innerHTML = "Valider";
					$("input[type=radio]").attr('disabled', false);
					self.status = 1;
					document.getElementById("noticeGroup").classList.toggle("hidden", true);
					self.quizApp.nextQuestion(self.trueAnswer);
				}
			}
		});
	}

	setQuestion(question) {
		var radios = '';

		this.questionContainer.text(question.prompt);

		question.forEachAnswer(function (answer, index) {
			radios +=
				'<li>' +
				'<input class="answer' + index +'" type="radio" name="answer" value="' + index + '" id="answer' + index + '"></input>' +
				'<label for="answer' + index + '">' + answer + '</label>' +
				'</li>';

		});

		this.answersContainer.html(radios);
	}

	toggle(hide) {
		this.element.toggleClass('hidden', hide);
	}
}