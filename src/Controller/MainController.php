<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use App\Entity\Contact;
use App\Entity\Salon;
use App\Entity\Recompense;
use App\Entity\Produit;
use App\Entity\Gite;

class MainController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function home()
    {
        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->redirect($this->generateUrl('home'));
    }

    /**
     * @Route("/news", name="news")
     */
    public function news()
    {
        $repo = $this->getDoctrine()->getRepository(Salon::class);
        $salons = $repo->findBy([], ['id' => 'DESC'], 4);

        $repo2 = $this->getDoctrine()->getRepository(Recompense::class);
        $recompenses = $repo2->findBy([], ['id' => 'DESC'], 8);

        return $this->render('main/news.html.twig', [
            'controller_name' => 'MainController',
            'salons' => $salons,
            'recompenses' => $recompenses
        ]);
    }

    /**
     * @Route("/workshop", name="workshop")
     */
    public function workshop()
    {
        return $this->render('main/workshop.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/advices", name="advices")
     */
    public function advices()
    {
        return $this->render('main/advices.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, \Swift_Mailer $mailer)
    {

        $repo = $this->getDoctrine()->getRepository(Gite::class);
        $gites = $repo->findBy([], ['id' => 'DESC'], 4);

        $contact = new Contact();

        $forms = $this->createFormBuilder($contact)
            ->add('name', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('mail', EmailType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('tel', NumberType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('sujet', TextType::class, [
                'attr' =>  [
                    'class' => 'form-control'
                ]
            ])
            ->add('message', TextareaType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->getForm();

        $forms->handleRequest($request);

        if ($forms->isSubmitted() && $forms->isValid()) {

            $name = $forms['name']->getData();
            $mail = $forms['mail']->getData();
            $tel = $forms['tel']->getData();
            $sujet = $forms['sujet']->getData();
            $message = $forms['message']->getData();

            # définir les données de formulaire   
            $contact->setName($name);
            $contact->setMail($mail);
            $contact->setSujet($sujet);
            $contact->setMessage($message);

            $message = (new \Swift_Message())
                ->setSubject($sujet)
                ->setFrom($mail)
                ->setTo('hidirkhimoum@gmail.com')
                ->setBody(
                    $this->renderView(
                        'emails/registration.html.twig',
                        ['name' => $message]),
                        'text/html');
            $mailer->send($message);
        }


        return $this->render('main/contact.html.twig', [
            'controller_name' => 'MainController',
            'gites' => $gites,
            'forms' => $forms->createView()
            
        ]);
    }

    /**
     * @Route("/philosophy", name="philosophy")
     */
    public function philosophy()
    {
        return $this->render('main/philosophy.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/shop", name="shop")
     */
    public function shop()
    {
        $repo = $this->getDoctrine()->getRepository(Produit::class);
        $produits = $repo->findBy([], ['id' => 'DESC']);

        return $this->render('main/shop.html.twig', [
            'controller_name' => 'MainController',
            'produits' => $produits,
        ]);
    }

    /**
     * @Route("/error", name="error")
     */
    public function error()
    {
        return $this->render('error/404.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/legals", name="legals")
     */
    public function legals()
    {
        return $this->render('main/legals.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }
}
