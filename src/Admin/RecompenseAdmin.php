<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;


final class RecompenseAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('cuvee', TextType::class, ['label' => 'Cuvée']);
        $formMapper->add('millesime', TextType::class, ['label' => 'Millésime / Autre', 'required'   => false]);
        $formMapper->add('gratification', TextType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('millesime');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('cuvee');
    }
}